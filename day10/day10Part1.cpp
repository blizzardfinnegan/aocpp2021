#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>

using namespace std;

int illegalPoints = 0;

void filterFunction(string rawInput)
{
    vector<char> currentLine;
        currentLine.clear();
        for(int index = 0; index < rawInput.length(); index++)
        {
            switch (rawInput[index])
            {
                case '[':
                case '(':
                case '{':
                case '<':
                    currentLine.push_back(rawInput[index]);
                    break;
                case ']':
                    if (currentLine.back() == '[')
                    {
                        currentLine.pop_back();
                    }
                    else
                    {
                        illegalPoints += 57;
                        return;
                    }
                    break;
                case ')':
                    if (currentLine.back() == '(')
                    {
                        currentLine.pop_back();
                    }
                    else
                    {
                        illegalPoints += 3;
                        return;
                    }
                    break;
                case '}':
                    if (currentLine.back() == '{')
                    {
                        currentLine.pop_back();
                    }
                    else
                    {
                        illegalPoints += 1197;
                        return;
                    }
                    break;
                case '>':
                    if (currentLine.back() == '<')
                    {
                        currentLine.pop_back();
                    }
                    else
                    {
                        illegalPoints += 25137;
                        return;
                    }
                    break;
                default:
                    cout << "Wrong input!" << endl;
                    cout << rawInput[index] << ",  index " << index << endl;
                    return;
            }
        }
}

int main(void)
{
    string rawInput;
    ifstream readFile ("data/input.txt", ios::in);
    while(getline(readFile, rawInput))
    {
        filterFunction(rawInput);
    }
    cout << "Total points: " << illegalPoints;
    return 0;
}
