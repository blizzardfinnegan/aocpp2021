#include <cmath>
#include <iostream>

using namespace std;

int main (void)
{
    int player1Position = 4; //This is for both test and true input
    int player1Score = 0;
    int player2Position = 5; //This is 5 for true, 8 for test
    int player2Score = 0;
    int dieRoll = 1;
    int turnAdd = 0;
    int rollCount = 1;
    bool player1Add = true;
    bool scoreAdd = false;
    int loserScore = 0;
    //Run the game loop until someone has the highest score 
    while ((player1Score < 1000) & (player2Score < 1000))
    {
        //If there's three values in the roll
        if(scoreAdd)
        {
            //If its player 1s turn, add their score
            if (player1Add)
            {
                player1Position = player1Position + turnAdd;
                player1Position = ((player1Position - 1) % 10) + 1;
                player1Score += player1Position;
                //Switch turn
                player1Add = false;
            }
            //It its player 2s turn, add their score
            else
            {
                player2Position = player2Position + turnAdd;
                player2Position = ((player2Position - 1) % 10) + 1;
                player2Score += player2Position;
                //Switch turn
                player1Add = true;
            }
            //Reset values
            scoreAdd = false;
            turnAdd = 0;
        }
        //Add until 3 values have been rolled
        else
        {
            //Implement rollover
            if(dieRoll > 100) dieRoll = 1;
            turnAdd += dieRoll;
            dieRoll++;
            rollCount++;
            if ((rollCount > 2) && ((rollCount % 3) == 1)) scoreAdd = true;
        }
    }
    //balance rollCount
    rollCount--;
    
    //Perform calculations based on winner
    if(!player1Add)
    {
        // If p1 wins, p2 is the loser
        loserScore = player2Score;
    }
    else
    {
        // If p2 wins, p1 is the loser
        loserScore = player1Score;
    }
    cout << "Final Score: " << loserScore * rollCount;
    return 0;
}
